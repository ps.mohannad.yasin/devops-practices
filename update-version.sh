#!/bin/bash

# Get the last commit date in YYMMDD format
LAST_COMMIT_DATE=$(git log -1 --format=%cd --date=format:'%y%m%d')

# Get the first 8 characters of the commit hash
COMMIT_HASH=$(git log -1 --format=%H | cut -c1-8)

# Formulate the version string
NEW_VERSION="${LAST_COMMIT_DATE}-${COMMIT_HASH}"

# Update the version in pom.xml using Maven Versions plugin
mvn versions:set -DnewVersion=${NEW_VERSION}
