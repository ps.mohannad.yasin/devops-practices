FROM openjdk:latest
WORKDIR /app
COPY ./target/*.jar /app/
ENV SPRING_PROFILES_ACTIVE=default
ENV JAVA_OPTS="-Xmx512m"
CMD ["sh", "-c", "java -jar -Dspring.profiles.active=${SPRING_PROFILES_ACTIVE} ${JAVA_OPTS} /app/*.jar"]
