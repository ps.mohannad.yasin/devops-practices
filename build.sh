#!/bin/bash
mvn clean install
# Ensure the script exits on error
set -e

# Get the last commit date and the commit hash for tagging the Docker image
LAST_COMMIT_DATE=$(git log -1 --format=%cd --date=format:'%y%m%d')
COMMIT_HASH=$(git log -1 --format=%H | cut -c1-8)

# Formulate the Docker image tag
IMAGE_TAG="${LAST_COMMIT_DATE}-${COMMIT_HASH}"

# Build the Docker image
docker build -t my-application:${IMAGE_TAG} .

# Display the Docker image tag
echo "Docker image built and tagged as: my-application:${IMAGE_TAG}"
